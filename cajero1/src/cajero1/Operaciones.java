/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajero1;


/**
 * 
 * @author Jose
 */
public abstract class Operaciones {

protected String Nombre;
protected int Contraseña;
protected int Saldo;
protected String TipodeCliente;
//Atributos a utilizar
    int Numero;

public Operaciones(String Nombre, int Contraseña, int Saldo, String TipodeCliente) {
this.Nombre = Nombre;
this.Contraseña = Contraseña;
this.Saldo = Saldo;
this.TipodeCliente = TipodeCliente;}

public Operaciones() {
this.Nombre = null;
this.Contraseña = 0;
this.Saldo = 0;
this.TipodeCliente = null;}

//Sobrecarga de constructores solo el primero vamos a utilizar los demás solo.
//se han creado para demostrar el principio de sobrecarga.

public abstract boolean Retiro(double Cantidad);
public abstract boolean Transferencia(double Cantidad);
public abstract boolean CompradeTiempoAire(double Cantidad);
public abstract boolean PagarLuz(double Cantidad);
public abstract boolean PagarAgua(double Cantidad);
public abstract void Consulta();
public abstract void info();
//Métodos abstractos
}